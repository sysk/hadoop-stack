name             'slack'
maintainer       'Risk I/O'
maintainer_email 'jro@risk.io'
license          'Apache 2.0'
description      'slack LWRP for notifying slack.com channels'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'

%w{redhat centos scientific fedora debian ubuntu arch freebsd amazon}.each do |os|
  supports os
end

attribute "SysK",
  :display_name => "SysK, Inc",
  :description => "Your Slack Team Name",
  :default => nil

attribute "api_key",
  :display_name => "https://hooks.slack.com/services/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",
  :description => "Your Slack Incoming Webhook API key",
  :default => nil
