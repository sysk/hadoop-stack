name             'rails-stack'
maintainer       'Conslog, Inc'
maintainer_email 'phelsilva@conslog.com.br'
license          'All rights reserved'
description      'Installs/Configures rails-stack'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'

depends			 'apt', '~> 4.0'