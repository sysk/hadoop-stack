rails-stack Cookbook
====================

This cookbook install all the prereqs for a RoR stack development with a few other necessary tools
in order to fullfil all basic gems requirements for Conslog, Inc



Requirements
------------
Ubuntu, MacOS

chef-client

Attributes
----------
TODO: List your cookbook attributes here.

e.g.
#### rails-stack::default
<table>
  <tr>
    <th>Key</th>
    <th>Type</th>
    <th>Description</th>
    <th>Default</th>
  </tr>
  <tr>
    <td><tt>['rails-stack']['bacon']</tt></td>
    <td>Boolean</td>
    <td>whether to include bacon</td>
    <td><tt>true</tt></td>
  </tr>
</table>

Usage
-----
#### rails-stack::default
TODO: Write usage instructions for each cookbook.

e.g.
Just include `rails-stack` in your node's `run_list`:

```json
{
  "name":"my_node",
  "run_list": [
    "recipe[rails-stack]"
  ]
}
```


License and Authors
-------------------
Authors: Claudio Fernando Maciel<claudio.fernando@gmail.com>
