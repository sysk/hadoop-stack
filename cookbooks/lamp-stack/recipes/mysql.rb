#mysqlpass = data_bag_item("mysql", "rtpass.json")

mysql_service "default" do
  port '3306'
  version '5.5'
  package_version ''
  initial_root_password 'spitz01'
  action [:create, :start]
end
