name             'lamp-stack'
maintainer       'Meanbiz'
maintainer_email 'cfernandomaciel@meanbiz.org'
license          'All rights reserved'
description      'Installs/Configures lamp-stack'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.1'

depends          'mysql', '~> 8.0'
depends			 'apt', '~> 4.0'
