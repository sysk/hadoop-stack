name             'hadoop-stack'
maintainer       'SysK, Inc'
maintainer_email 'claudio.fernando@mgail.com'
license          'All rights reserved'
description      'Installs/Configures hadoop-stack'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'

depends			 'slack', '~> 0.1'
#depends			 'apt', '~> 4.0'
#depends			 'opencv', '~> 0.0'