hadoop-stack Cookbook
=====================

Installation of the entire stack for SysK, Inc POCs on Hadoop with OpenCV

Requirements
------------
ubuntu 14.04
chef-client

e.g.
#### packages
- `toaster` - hadoop-stack needs toaster to brown your bagel.

Attributes
----------
TODO: List your cookbook attributes here.

e.g.
#### hadoop-stack::default
<table>
  <tr>
    <th>Key</th>
    <th>Type</th>
    <th>Description</th>
    <th>Default</th>
  </tr>
  <tr>
    <td><tt>['hadoop-stack']['bacon']</tt></td>
    <td>Boolean</td>
    <td>whether to include bacon</td>
    <td><tt>true</tt></td>
  </tr>
</table>

Usage
-----
#### hadoop-stack::default
TODO: Write usage instructions for each cookbook.

e.g.
Just include `hadoop-stack` in your node's `run_list`:

```json
{
  "name":"my_node",
  "run_list": [
    "recipe[hadoop-stack]"
  ]
}
```


License and Authors
-------------------
Authors: CLaudio Fernando Maciel<claudio.fernando@gmail.com>
